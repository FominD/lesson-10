<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core_functions/template_functions.php' ;

$seo = [
    'title' => 'Hillel blog about PHP',
    'desciprtion' => 'You can find amazing notes about PHP'
];

$data = [
    'seo' => $seo
];


template('blog/section', $data);
